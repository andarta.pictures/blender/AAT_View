import bpy
import json

from .common.utils import register_classes, unregister_classes
from .common.blender_ui import show_message

from . import core


    
class AAT_VP_flip_viewport(bpy.types.Operator) :
    bl_idname = "gpencil.flip_viewport"
    bl_label = "flip the viewport"
    bl_options = {'REGISTER', 'UNDO'}

    
    @classmethod
    def poll(cls, context):
        return context.region_data.view_perspective == 'CAMERA'
    
    def execute(self, context):
               
        #SAVE START CONTEXT
        current_mode = bpy.context.object.mode
        current_object = bpy.context.object
        

        cam_ob = context.scene.camera
        is_fliped = cam_ob.get('IS_FLIPED')
        
        if not is_fliped:
            cam_ob['IS_FLIPED'] = True
            is_rotated = cam_ob.get('IS_ROTATED')
            if not is_rotated:
                core.viewport_alter_display_switch(cam_ob['IS_FLIPED'])
                core.trace_frame(core.get_frame_stroke())
                
        else:
            cam_ob['IS_FLIPED'] = False
            is_rotated = cam_ob.get('IS_ROTATED')
            if not is_rotated:
                    core.viewport_alter_display_switch(cam_ob['IS_FLIPED'])

        # viewport_alter_display_switch(cam_ob['IS_FLIPED'] or cam_ob['IS_ROTATED'] )
        bpy.context.scene.camera.scale[0] *= -1

        #RESTORE
        bpy.context.view_layer.objects.active = current_object
        # print('re-setting mode:',current_mode)
        if bpy.context.object.mode != current_mode:
            bpy.ops.object.mode_set ( mode = current_mode )

        return {'FINISHED'}
      




class AAT_VP_reset_viewport(bpy.types.Operator) :
    bl_idname = "gpencil.reset_viewport"
    bl_label = "reset the viewport"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        # return truue if ui_gp_name object exist
        if bpy.context.scene.objects.get("UI_FRAME"):
            return True
        return False
        

    def execute(self, context):      
        core.reset_camera(context)
        return {'FINISHED'}


class AAT_VP_toggle_bg_img(bpy.types.Operator) :
    bl_idname = "gpencil.toogle_bg_image_property"
    bl_label = "Toggle Camera BG image property"
    bl_options = {'REGISTER', 'UNDO'}

    data : bpy.props.StringProperty(default="")
        

    def execute(self, context): 
        try :
            cam_name, i, prop_name = json.loads(self.data)
        except : 
            return {'CANCELLED'}
        
        print("Toggling : ", self.data)

        cam = bpy.data.objects[cam_name]
        bg_img = cam.data.background_images[i]

        if prop_name == "show_background_image" :
            bg_img.show_background_image = not bg_img.show_background_image
        
        if prop_name == "display_depth" :
            if bg_img.display_depth == "FRONT" :
                bg_img.display_depth = "BACK"
            elif bg_img.display_depth == "BACK" :
                bg_img.display_depth = "FRONT"
        
        return {'FINISHED'}
    


class AAT_VP_rotate_canvas(bpy.types.Operator) :
    bl_idname = "gpencil.rotate_canvas"
    bl_label = "rotate the viewport and redraw passepartout"
    bl_options = {'REGISTER', 'UNDO'}


    def execute(self, context): 

        #SAVE START CONTEXT
        current_mode = bpy.context.object.mode
        current_object = bpy.context.object

        # based on greaspencil tools set:
        cam_ob = context.scene.camera

        lock_state = core.unlock_camera(cam_ob)

        is_rotate_alter = cam_ob.get('IS_ROTATED')
        if not is_rotate_alter:
            cam_ob['IS_ROTATED'] = True            
            cam_ob['stored_rotation'] = cam_ob.rotation_euler
            if not cam_ob.get('_RNA_UI'):
                cam_ob['_RNA_UI'] = {}
                cam_ob['_RNA_UI']["stored_rotation"] = {
                        "description":"Stored camera rotation (Gpencil tools > rotate canvas operator)",
                        "subtype":'EULER',
                        # "is_overridable_library":0,
                        }   

            # Usefull until there is a solution to get angle
            #bpy.ops.viewport.reset_viewport('INVOKE_DEFAULT')
            core.viewport_alter_display_switch (True)
                            
            # draw camera frame           
            core.trace_frame(core.get_frame_stroke())
    
        # Launch rotate_canvas    
        try: 
            bpy.ops.view3d.rotate_canvas('INVOKE_DEFAULT')
        
        except:
            self.report({"WARNING"}, "GP tools aren't activated!\n  Please go in Edit>Preferences>Add-ons and activate 'Object: Grease Pencil Tools' add-on")
            show_message("GP tools aren't activated! Please go in Edit>Preferences>Add-ons and activate 'Object: Grease Pencil Tools' add-on", "WARNING")
            return {"CANCELLED"}

        finally :
            #RESTORE
            bpy.context.view_layer.objects.active = current_object
            # print('re-setting mode:',current_mode)
            #if mode is different from previous set to current mode 
            if bpy.context.object.mode != current_mode:
                bpy.ops.object.mode_set ( mode = current_mode )
            
            core.restore_lock_state(cam_ob, lock_state)

        return {'FINISHED'}


classes = [
    AAT_VP_flip_viewport,
    AAT_VP_reset_viewport,
    AAT_VP_rotate_canvas,
    AAT_VP_toggle_bg_img,
]

def register() :
    register_classes(classes)

def unregister() :
    unregister_classes(classes)


import bpy
from .common.gp_utils_bis import get_grease_pencil_layer,get_grease_pencil
from .common.utils_bis import object_to_collection, view3d_camera_border
import mathutils



ui_gp_name = "UI_FRAME"

def viewport_alter_display_switch (is_alter :bool):
    # get passepartout info and hide
    # bpy.context.scene.camera.data.show_passepartout = not is_alter

    # hide dotted line
    for area in bpy.context.screen.areas:
                if area.type == 'VIEW_3D':
                    for space in area.spaces:
                        if space.type == 'VIEW_3D':
                            space.overlay.show_annotation= not is_alter
                            break
    gp_ui = bpy.context.scene.objects.get("UI_FRAME")

    if gp_ui:
        gp_ui.hide_viewport = not is_alter

def get_frame_stroke(gp_name = ui_gp_name ,clear = False):        
    
    
    gp,isNew = get_grease_pencil(gp_name)
    
    if isNew:
        object_to_collection(gp, 'COL_TMP')

    gpl = get_grease_pencil_layer(gp, gpencil_layer_name='FRAME',clear_layer=clear)    

    if isNew:
        material = bpy.data.materials.new("UI_MATERIAL")
        bpy.data.materials.create_gpencil_data(material)
        material.grease_pencil.color = (1, 0, 0, 1) # color red
    else:
        material = bpy.data.materials.get("UI_MATERIAL")
    
    gp.active_material = material
    

    #CONSTRAINT
    #set current curve as selected  
    # bpy.ops.object.constraint_add(type='COPY_LOCATION')
    # bpy.ops.object.constraint_add(type='COPY_ROTATION')
    # context.object.constraints["Copy Location"].target = bpy.data.objects["Camera"]
    # context.object.constraints["Copy Rotation"].target = bpy.data.objects["Camera"]


    scene = bpy.context.scene
    scene.grease_pencil = gp.data
    current_frame = scene.frame_current

    # Reference active GP frame or create one if none exists    
    if gpl.frames:
        fr = gpl.active_frame
    else:
        fr = gpl.frames.new(current_frame) 

    #Get Stroke
    if fr.strokes:
        stroke = fr.strokes[0]
    else:
        stroke = fr.strokes.new()
        #stroke.draw_mode = '3DSPACE' # either of ('SCREEN', '3DSPACE', '2DSPACE', '2DIMAGE')

        strokeLength = 4
        stroke.points.add(count = strokeLength )
        fr.strokes.close(stroke)



    return stroke

def trace_frame(stroke):


    frame_px,frame = view3d_camera_border(bpy.context.scene)
    # print("Camera frame:", frame_px)
    points = stroke.points
    for i,point in  enumerate(frame_px):
        points[i].co = frame[i]
    if len(points)==5:
        points[4].co = points[0].co



def reset_camera(context=None) :
    
    if context is None :
        context = bpy.context

    cam_ob = context.scene.camera
    lock_state = unlock_camera(cam_ob)

    is_rotate_alter = cam_ob.get('IS_ROTATED')
    is_fliped = cam_ob.get('IS_FLIPED')
    if is_rotate_alter:
        cam_ob.rotation_euler = cam_ob['stored_rotation'] 
        cam_ob.scale[0] = abs(cam_ob.scale[0])
        cam_ob.scale[1] = abs(cam_ob.scale[1])
        cam_ob.scale[2] = abs(cam_ob.scale[2])

        # Reset viewport roll 
        if type(context.region_data) != type(None):
            in_cam = context.region_data.view_perspective == 'CAMERA'
            if not in_cam:
                aim = context.space_data.region_3d.view_rotation @ mathutils.Vector((0.0, 0.0, 1.0))#view vector
                z_up_quat = aim.to_track_quat('Z','Y')#track Z, up Y
                context.space_data.region_3d.view_rotation = z_up_quat            

    elif is_fliped:
        bpy.context.scene.camera.scale[0] = 1.0 
        # cam_ob['IS_FLIPED']= False
    #Restore Display
    
    viewport_alter_display_switch (False)
    #empty gp
    # gp_stroke = get_frame_stroke(clear=True)

    clear_tmp()
    
        
        
    obj = bpy.data.objects.get(ui_gp_name)
    if obj:
        #remove all the layers
        for layer in obj.data.layers:
            obj.data.layers.remove(layer)
        bpy.data.objects.remove(obj, do_unlink=True)

    cam_ob['IS_FLIPED']= False
    cam_ob['IS_ROTATED']= False

    restore_lock_state(cam_ob, lock_state)


def unlock_camera(cam) :
    lock_state = []

    for i in range(0,3) :
        lock_state.append(cam.lock_rotation[i])
        cam.lock_rotation[i] = False
    
    return lock_state


def restore_lock_state(cam, lock_state) :
    for i in range(0,3) :
        cam.lock_rotation[i] = lock_state[i]


def clear_tmp() :
    for o in bpy.data.objects : 
        if o.name == "UI_FRAME" : 
            bpy.data.objects.remove(o)

    scene_col = bpy.context.scene.collection
    for col in scene_col.children : 
        if col.name.startswith("COL_TMP") :
            if len(col.all_objects) == 0 :
                bpy.data.collections.remove(col)
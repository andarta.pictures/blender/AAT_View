
import bpy
import traceback
from bpy.app.handlers import persistent

from . import core


def reset_rotation():
    cam = bpy.context.scene.camera
    if cam:
        is_rotate_alter = cam.get('IS_ROTATED')
        is_fliped = cam.get('IS_FLIPED')
        if is_rotate_alter or is_fliped:
            try :
                bpy.ops.viewport.reset_viewport('INVOKE_DEFAULT')
            except AttributeError as e :
                pass


@persistent
def frame_change_handler(scene):
    return


@persistent
def reset_view(scene):
    core.reset_camera()
    reset_rotation()


def register() :
    bpy.app.handlers.save_pre.append(reset_view)
    bpy.app.handlers.frame_change_pre.append(frame_change_handler)
    bpy.app.handlers.frame_change_post.append(frame_change_handler)


def unregister() :
    bpy.app.handlers.frame_change_post.remove(frame_change_handler)
    bpy.app.handlers.frame_change_pre.remove(frame_change_handler),
    bpy.app.handlers.save_pre.remove(reset_view)
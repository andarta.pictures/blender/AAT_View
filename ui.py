
import bpy
import json

class View_Panel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "Viewport options"
    bl_idname = "OBJECT_PT_viewpanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"

    show_bg_images : bpy.props.BoolProperty(default=False)


    def draw(self, context):

        cam_ob = bpy.context.scene.camera
        if cam_ob:
            row = self.layout.row()
            row.label(text="Passe-partout : ")
            row.prop(context.scene.camera.data, "show_passepartout", text="")
            row.prop(context.scene.camera.data, "passepartout_alpha", text="")
            row = self.layout.row()
            row.operator("gpencil.flip_viewport", text="flip")
            row.operator("gpencil.rotate_canvas", text="rotate")
            row.operator("gpencil.reset_viewport", text="reset")
            
            def bg_name(bg_img) :
                if bg_img.clip is not None :
                    return bg_img.clip.name
                elif bg_img.image is not None :
                    return bg_img.image.name

            valid_bg_images = [(i, im) for i, im in enumerate(cam_ob.data.background_images) if im.clip is not None or im.image is not None]
            if len(valid_bg_images) == 0 : 
                return 
            
            self.layout.separator()
            self.layout.label(text="Background images :")

            for i,bg_img in valid_bg_images :
                pack_info = lambda prop_name : json.dumps([cam_ob.name,i,prop_name])

                row = self.layout.row(align=True)
                row.prop(bg_img, "alpha", text=bg_name(bg_img), slider=True)

                icon = "HIDE_OFF" if bg_img.show_background_image else "HIDE_ON"
                row.operator("gpencil.toogle_bg_image_property", text="", icon=icon).data = pack_info("show_background_image")

                icon = "TRIA_UP" if bg_img.display_depth == "FRONT" else "TRIA_DOWN"
                row.operator("gpencil.toogle_bg_image_property", text="", icon=icon).data = pack_info("display_depth")



def register() :
    bpy.utils.register_class(View_Panel)

def unregister() :
    bpy.utils.unregister_class(View_Panel)

import bpy
from typing import Optional
from mathutils import Vector
import numpy as np

from . import mode_manager

def get_active_gp_object() -> Optional[bpy.types.Object]:
    """Get the active grease pencil object if any, None otherwise."""
    active_obj = bpy.context.active_object
    if not active_obj or not isinstance(active_obj.data, bpy.types.GreasePencil):
        return None
    return active_obj

def get_all_gp_objects() :
    """Returns a list of all Grease Pencil objects."""

    gp_objects = []

    for o in bpy.data.objects :
        if isinstance(o.data, bpy.types.GreasePencil) :
            gp_objects.append(o)

    return gp_objects 

def get_scene_gp_objects() :
    """Returns a list of all Grease Pencil objects."""
    
    scene = bpy.context.scene
    gp_objects = []

    for o in scene.objects :
        if isinstance(o.data, bpy.types.GreasePencil) :
            gp_objects.append(o)

    return gp_objects 

def select_all_gp_geometry(gp_obj, multiedit=False) :
    """Select every stroke for every layer in a Grease Pencil object.
    
    - gp_obj : Grease Pencil to select
    - multiedit : if True, also selects every stroke for every frame selected, if False, only on active frame"""

    layers = gp_obj.data.layers
    
    for l in layers :
        if l.lock or l.hide or not l.active_frame :
            continue
        
        if multiedit:
            target_frames = [f for f in l.frames if f.select]
        else:
            target_frames = [l.active_frame]

        for f in target_frames:
            for s in f.strokes:
                s.select = True

def get_active_gp_layer_framelist() :
    """Returns a list containing the time position of every frame in the Grease Pencil"""
    active_gp = get_active_gp_object()
    frame_n = []

    if active_gp is not None :
        layers = active_gp.data.layers
        for f in layers.active.frames :
            frame_n.append(f.frame_number)
        frame_n.sort()
    
    return frame_n

def get_object_depth(gp_object) :
    """Returns Y dimension of the mean point position of a given gp_object.
    
    WARNING : can be slow on high amount of points"""

    geo = get_geo(gp_object)
    geo = np.array(geo)
    centroid = np.mean(geo, axis=0)
    return float(centroid[1])

def sort_by_depth(gp_object_list) :
    """Sorts given gp_object_list by depth using get_object_depth()
    
    WARNING : can be slow on high amount of points"""

    depth = [get_object_depth(o) for o in gp_object_list]
    order = np.array(depth).argsort()
    return [gp_object_list[i] for i in reversed(order)]

def get_geo(obj, stroke_length_threshold=None, current_frame_only=False) :
    """Returns a list of points coordinates in the geometry of selected GP elements."""
    coords = []
    layers = obj.data.layers

    for l in layers :
        frames = [l.active_frame] if current_frame_only else l.frames
        for f in frames:
            for s in f.strokes:

                if stroke_length_threshold is not None :
                    assert isinstance(stroke_length_threshold, int), "stroke_length_threshold should be either None or an int"
                    if len(s.points) < stroke_length_threshold :
                        continue
                        
                for p in s.points:
                        coords.append(obj.matrix_world @ p.co)

    if len(coords) == 0 :
        coords.append(obj.matrix_world @ Vector((0,0,0)))

    return coords

def get_collection_list() :
    """Returns a list of every collection in the project."""
    scene = bpy.context.scene
    collecs = [c for c in bpy.data.collections] + [scene.collection]
    return collecs

def get_parent_collection_list(obj) :
    collection_list = []
    
    for c in bpy.data.collections :
        if obj.name in [o.name for o in c.all_objects] :
            collection_list.append(c)
    
    return collection_list
        

def get_layer_collection(collection, view_layer=None):
    '''Returns the view layer LayerCollection for a specificied Collection'''
    
    def scan_children(lc, result=None):
        for c in lc.children:
            if c.collection == collection:
                return c
            result = scan_children(c, result)
        return result

    if view_layer is None:
        view_layer = bpy.context.view_layer
    
    return scan_children(view_layer.layer_collection)

def is_collection_hierarchy_active(obj) :
    parent_col_list = get_parent_collection_list(obj)
    excluded = [get_layer_collection(c).exclude for c in parent_col_list]
    return True not in excluded


def refresh_geometry_display(object=None) :
    og_obj = bpy.context.view_layer.objects.active

    mode = mode_manager.switch_mode('OBJECT')
    if object is not None :
        bpy.context.view_layer.objects.active = object

    mode_manager.switch_mode('EDIT_GPENCIL')

    bpy.ops.gpencil.select_all(action='SELECT')
    bpy.ops.transform.translate(value=(.0, .0, .0), 
                                orient_type='GLOBAL', 
                                orient_matrix_type='GLOBAL', mirror=False, 
                                )
    
    mode_manager.switch_mode('OBJECT')
    bpy.context.view_layer.objects.active = og_obj

    mode.switch()

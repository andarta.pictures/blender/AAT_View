# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy


def set_gpencil_mode_safe(
    context: bpy.types.Context, gpencil: bpy.types.Object, mode: str
):
    """
    Safely set the interaction `mode` on `gpencil` by handling inconsistencies
    between object mode and internal gpencil data flags that could lead to errors.

    :param context: The current context
    :param gpencil: The grease pencil object to set the mode of
    :param mode: The mode to activate
    """
    # Override context: "object.mode_set" uses view_layer's active object
    with context.temp_override(
        scene=context.window.scene,
        view_layer=context.window.scene.view_layers[0],
        active_object=gpencil,
    ):
        # There might be inconsistencies between the object interaction mode and
        # the mode flag on the gp data itself (is_stroke_{modename}_mode).
        # When that happens, switching to the mode fails.
        # If data flag for this mode is already activated, switch to the safe
        # 'EDIT_GPENCIL' mode first as a workaround to sync back the flags.
        if mode not in ("OBJECT", "EDIT_GPENCIL"):
            # Get short name for this mode to build data flag
            # e.g: PAINT_GPENCIL => paint
            mode_short = mode.split("_")[0].lower()
            # Switch to edit mode first if data flags is activated
            if getattr(gpencil.data, f"is_stroke_{mode_short}_mode", False):
                bpy.ops.object.mode_set(mode="EDIT_GPENCIL")
        # Switch the object interaction mode
        bpy.ops.object.mode_set(mode=mode)

def safe_mode_set(mode) :
    if bpy.context.mode == mode :
        return
    bpy.ops.object.mode_set(mode=mode)

class ModeSaver :

    def __init__(self) :
        self.name = bpy.context.mode
        self.active_object = self.get_active_object()
    
    def get_active_object(self) :
        if "active_object" in dir(bpy.context) :
            return bpy.context.active_object
        return None

    def switch(self, restore_active_object=False) :
        print("Switching back to ", self.name)

        if restore_active_object and self.active_object is not None :
            bpy.context.view_layer.objects.active = self.active_object
        
        safe_mode_set(self.name)


def switch_mode(mode) :
    """Switches to the specified Blender mode, but returns an object
    that can restore the original mode.
    
    e.g. : 

    og_mode = switch_mode('EDIT_GPENCIL') ; ... ; og_mode.switch()"""


    previous_mode = ModeSaver()
    # print("Trying to switch to ", mode)
    safe_mode_set(mode)
    return previous_mode
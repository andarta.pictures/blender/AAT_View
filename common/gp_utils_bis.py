import bpy


# based on https://towardsdatascience.com/blender-2-8-grease-pencil-scripting-and-generative-art-cbbfd3967590
def get_grease_pencil(gpencil_obj_name='GPencil',
                      hide_selection = True, 
                      hide_render = True, 
                      hide_viewport = False
                      ) -> bpy.types.GreasePencil:
    """
    Return the grease-pencil object with the given name. Initialize one if not already present.
    """

    isNew = False
    bpy.ops.object.mode_set(mode='OBJECT')
    # If not present already, create grease pencil object
    gpencil = bpy.context.scene.objects.get(gpencil_obj_name)
    
    if not gpencil:
        
        #bpy.ops.object.gpencil_add(view_align=False, location=(0, 0, 0), type='EMPTY')
        bpy.ops.object.gpencil_add(location=(0, 0, 0), type='EMPTY')
        # rename grease pencil
        gpencil = bpy.context.object
        gpencil.name = gpencil_obj_name
        isNew = True
        gpencil.hide_select = hide_selection
        gpencil.hide_render = hide_render

        #bpy.context.scene.objects[-1].name = gpencil_obj_name

    gpencil.hide_viewport = hide_viewport

    return gpencil,isNew


def get_grease_pencil_layer(gpencil: bpy.types.GreasePencil, gpencil_layer_name='GP_Layer',
                            clear_layer=False) -> bpy.types.GPencilLayer:
    """
    Return the grease-pencil layer with the given name. Create one if not already present.
    :param gpencil: grease-pencil object for the layer data
    :param gpencil_layer_name: name/key of the grease pencil layer
    :param clear_layer: whether to clear all previous layer data
    """

    # Get grease pencil layer or create one if none exists
    if gpencil.data.layers and gpencil_layer_name in gpencil.data.layers:
        gpencil_layer = gpencil.data.layers[gpencil_layer_name]
    else:
        gpencil_layer = gpencil.data.layers.new(gpencil_layer_name, set_active=True)

    if clear_layer:
        gpencil_layer.clear()  # clear all previous layer data

    # bpy.ops.gpencil.paintmode_toggle()  # need to trigger otherwise there is no frame

    return gpencil_layer


# Util for default behavior merging previous two methods
def init_grease_pencil(gpencil_obj_name='GPencil', gpencil_layer_name='GP_Layer',
                       clear_layer=True) -> bpy.types.GPencilLayer:
    gpencil = get_grease_pencil(gpencil_obj_name)
    gpencil_layer = get_grease_pencil_layer(gpencil, gpencil_layer_name, clear_layer=clear_layer)
    return gpencil_layer

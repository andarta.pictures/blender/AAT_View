import bpy
from bpy_extras.view3d_utils import location_3d_to_region_2d
import gpu
from gpu_extras.batch import batch_for_shader

'''
https://blender.stackexchange.com/questions/6377/coordinates-of-corners-of-camera-view-border
Thanks to ideasman42
'''
def view3d_find():
    # returns first 3d view, normally we get from context
    for area in bpy.context.window.screen.areas:
        if area.type == 'VIEW_3D':
            v3d = area.spaces[0]
            rv3d = v3d.region_3d
            for region in area.regions:
                if region.type == 'WINDOW':
                    return region, rv3d
    return None, None

def view3d_camera_border(scene):
    obj = scene.camera
    cam = obj.data

    frame = cam.view_frame(scene=scene)

    # move from object-space into world-space 
    frame = [obj.matrix_world @ v for v in frame]

    # move into pixelspace   
    region, rv3d = view3d_find()
    frame_px = [location_3d_to_region_2d(region, rv3d, v) for v in frame]
    return frame_px,frame


def get_collection(target_name) : 
    names = lambda l : [e.name for e in l]

    scene = bpy.context.scene 
    if target_name in bpy.data.collections.keys() : 
        col = bpy.data.collections[target_name]
    
    else : 
        col = bpy.data.collections.new(target_name) 
    
    if col.name not in names(scene.collection.children_recursive) :
        scene.collection.children.link(col)

    return col    


def object_to_collection (obj, target_name):
    for c in  obj.users_collection:
        c.objects.unlink(obj)

    target_col = get_collection(target_name)
    target_col.objects.link(obj)


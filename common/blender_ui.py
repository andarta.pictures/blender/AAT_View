# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy

def show_message(message = "", title = "Message Box", icon = 'INFO'):
    """Displays small message box to user.
    
    - message : message to be displayed
    - title : title of the message box
    - icon : icon to be display ('INFO', 'WARNING', 'ERROR')"""
    
    def draw(self, context):
        self.layout.label(text=message)

    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)


def full_redraw() :
    for area in bpy.context.screen.areas: 
        area.tag_redraw()

ignore_next_mouse_move = 0

def cycle_mouse(mouse_position, region=None) :
    """Allows to teleport the cursor to opposite side of the view region when it exits it.
    
    - mouse_position : (x,y) coordinates of the mouse from event
    - region : view region, can usually be found via context.region
    
    Returns new mouse_position if it needs to be stored."""

    global ignore_next_mouse_move
    
    if region is None :
        area  = [area for area in bpy.context.window.screen.areas if area.type == 'VIEW_3D'][0]
        region = [region for region in area.regions if region.type == "WINDOW"][0]
    
    mouse_x, mouse_y = mouse_position

    if ignore_next_mouse_move == 0 :
        new_mouse_x = mouse_x%region.width
        new_mouse_y = mouse_y%region.height
        mouse_changed = mouse_x != new_mouse_x or mouse_y != new_mouse_y

        if mouse_changed :
            bpy.context.window.cursor_warp(new_mouse_x + region.x, new_mouse_y + region.y)
            ignore_next_mouse_move = 2

    else :
        ignore_next_mouse_move += -1

    return (mouse_x, mouse_y)